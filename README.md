# Kunpeng-Competition-2022

#### 介绍
鲲鹏应用创新大赛2022（湖北赛区），中国地质大学（武汉）提交参赛作品：基于特定域语言和鲲鹏HPC使能的计算流体力学模型开发与应用，本仓库中包含了OP2, MGCFD-OP2的运行demo（包括可执行程序和输入数据文件）, 使用Devkit工具的软件迁移分析，源码迁移分析和性能优化分析；使用多瑙调度器在鲲鹏集群上运行HPC应用。

#### 目录说明
## demo
包含运行m6_wing算例的输入数据文件和可执行程序及其需要的so文件。运行步骤见demo目录下的运行说明。

## devkit_analysis
对OP2库和MGCFD-OP2_app程序的源码包使用鲲鹏Devkit工具做的源码迁移和运行调优分析，具体见目录下的html扫描报告和加分举证说明。

## P2P_HPX_async_MPI
使用HPX async MPI改造的点对点异步通信与传统的MPI非阻塞通信，计算矩阵相乘的示例算例代码。

## validated_cerficate
根据华为公司的validated要求，撰写的"展翅报告.docx"

## donau-scripts
在鲲鹏集群上，运行MPI+X并行化的MGCFD应用，使用的多瑙调度器的运行脚本，包含airfoil和Rotor两个场景。

## OP2-issue
我对OP2开发的一些github提问。

## 鲲鹏应用创新大赛2022方案介绍-中国地质大学（武汉）.pptx
## 加分举证证明.docx

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
