#include <mpi.h>
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <array>

#include <hpx/local/execution.hpp>
#include <hpx/local/future.hpp>
#include <hpx/local/init.hpp>
#include <hpx/modules/async_mpi.hpp>
#include <hpx/modules/testing.hpp>
#include <hpx/program_options.hpp>

using hpx::program_options::options_description;
using hpx::program_options::value;
using hpx::program_options::variables_map;

double *a, *b, *c;                      /* Data blocks init in root */
double *a_block, *b_block, *c_block;    /* Blocks to calculate on each process */

// this is called on an hpx thread after the runtime starts up
int hpx_main(hpx::program_options::variables_map& vm)
{
    const std::uint64_t N = vm["matrixsize"].as<std::uint64_t>();      
    const int block_size = N/2;
    const int num_block_elements = block_size * block_size;

    int world_size, world_rank;
         
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
      
    // this needs to scope all uses of hpx::mpi::experimental::executor  
   // printf("Start executor\n");
    hpx::mpi::experimental::enable_user_polling enable_polling;    
    hpx::mpi::experimental::executor exec(MPI_COMM_WORLD);
    
    if (world_size != 4) {
        if (world_rank == 0) {
            printf("Need 4 processes\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        MPI_Finalize();
        exit(1);
    }
    
    if (world_rank == 0) {
        a = alloc_matrix(N);
        b = alloc_matrix(N);
        c = alloc_matrix(N);
        init_matrix(a, N);
        init_matrix(b, N);
        fill_matrix(c, N, 0);
    }

    /* Start of parallel section */
    MPI_Barrier(MPI_COMM_WORLD);
    double time = - MPI_Wtime();  // start timing

    a_block = alloc_matrix(block_size);
    b_block = alloc_matrix(block_size);
    c_block = alloc_matrix(block_size);
    fill_matrix(a_block, block_size, 0);
    fill_matrix(b_block, block_size, 0);
    fill_matrix(c_block, block_size, 0);

    MPI_Datatype array_block;
    if (world_rank == 0) {
        int sizes[2] = {N, N};
        int subsizes[2] = {block_size, block_size};
        int starts[2] = {0, 0};
        MPI_Type_create_subarray(2, sizes, subsizes, starts,
            MPI_ORDER_C, MPI_DOUBLE, &array_block);

        int double_size;
        MPI_Type_size(MPI_DOUBLE, &double_size);
        MPI_Type_create_resized(array_block, 0, 1*double_size, &array_block);
        MPI_Type_commit(&array_block);
    }
    
    /********************************************************/
    /* Do multiplications on submatrices */
    /*
            A               B
        | 0 | 1 |       | 0 | 1 |
        ---------       ---------
        | 2 | 3 |       | 2 | 3 |

    */
    const int blocks[4] = {         /* Memory address of top-left corners */
        0, block_size,              /*     of blocks 0, 1, 2, 3 */
        N*block_size, N*block_size + block_size
    };

    /* First pass:
        Proc 0: a0 * b0
        Proc 1: a0 * b1
        Proc 2: a2 * b0
        Proc 3: a3 * b1 */
   // printf("First pass\n");
    {
        const int a_tag = 0, b_tag = 1;
        if (world_rank == 0) {
            
            // TEST: using hpx::async array
            //std::array<hpx::future<int>,6> f_send{};
            //f_send.at(0)=hpx::async(exec, MPI_Isend, &a[blocks[0]], 1, array_block, 1, a_tag);
            
            hpx::future<int> f_send0 = hpx::async(exec, MPI_Isend, &a[blocks[0]], 1, array_block, 1, a_tag);      /* a0 --> proc 1 */
            hpx::future<int> f_send1 = hpx::async(exec, MPI_Isend, &a[blocks[2]], 1, array_block, 2, a_tag);      /* a2 --> proc 2 */
            hpx::future<int> f_send2 = hpx::async(exec, MPI_Isend, &a[blocks[2]], 1, array_block, 3, a_tag);      /* a2 --> proc 3 */
            
            hpx::future<int> f_send3 = hpx::async(exec, MPI_Isend, &b[blocks[1]], 1, array_block, 1, b_tag);      /* b1 --> proc 1 */
            hpx::future<int> f_send4 = hpx::async(exec, MPI_Isend, &b[blocks[0]], 1, array_block, 2, b_tag);      /* b0 --> proc 2 */
            hpx::future<int> f_send5 = hpx::async(exec, MPI_Isend, &b[blocks[1]], 1, array_block, 3, b_tag);      /* b1 --> proc 3 */


            /* proc 0: copy a0, b0 --> a_block, b_block */
            int i, j;
            for (i = 0; i < block_size; i++) {
                for (j = 0; j < block_size; j++) {
                    a_block[i*block_size + j] = a[blocks[0] + i*N + j];
                    b_block[i*block_size + j] = b[blocks[0] + i*N + j];
                }
            }

        }
        else {
            auto result0 = hpx::async(exec, MPI_Irecv, a_block, num_block_elements, MPI_DOUBLE, 0, a_tag);
            auto result1 = hpx::async(exec, MPI_Irecv, b_block, num_block_elements, MPI_DOUBLE, 0, b_tag);
            hpx::wait_all(result0, result1);
        }
    }


    block_multiply(a_block, b_block, c_block, block_size);
    MPI_Barrier(MPI_COMM_WORLD);     // Every slave procs have to be done before `a_block` can be reused
    
    /*  Second pass:
            Proc 0: a1 * b2
            Proc 1: a1 * b3
            Proc 2: a3 * b2
            Proc 3: a3 * b3 */
    // printf("Second pass\n");
    {
        const int a_tag = 0, b_tag = 1;
        if (world_rank == 0) {
        
            hpx::future<int> f_send0 = hpx::async(exec, MPI_Isend, &a[blocks[1]], 1, array_block, 1, a_tag);    /* a0 --> proc 1 */
            hpx::future<int> f_send1 = hpx::async(exec, MPI_Isend, &a[blocks[3]], 1, array_block, 2, a_tag);    /* a2 --> proc 2 */
            hpx::future<int> f_send2 = hpx::async(exec, MPI_Isend, &a[blocks[3]], 1, array_block, 3, a_tag);    /* a2 --> proc 3 */
            
            hpx::future<int> f_send3 = hpx::async(exec, MPI_Isend, &b[blocks[3]], 1, array_block, 1, b_tag);    /* b1 --> proc 1 */
            hpx::future<int> f_send4 = hpx::async(exec, MPI_Isend, &b[blocks[2]], 1, array_block, 2, b_tag);    /* b0 --> proc 2 */
            hpx::future<int> f_send5 = hpx::async(exec, MPI_Isend, &b[blocks[3]], 1, array_block, 3, b_tag);    /* b1 --> proc 3 */


            /* proc 0: copy a1, b2 --> a_block, b_block */
            int i, j;
            for (i = 0; i < block_size; i++) {
                for (j = 0; j < block_size; j++) {
                    a_block[i*block_size + j] = a[blocks[1] + i*N + j];
                    b_block[i*block_size + j] = b[blocks[2] + i*N + j];
                }
            }

        }
        else {
        
            auto result0 =  hpx::async(exec, MPI_Irecv, a_block, num_block_elements, MPI_DOUBLE, 0, a_tag);
            auto result1 =  hpx::async(exec, MPI_Irecv, b_block, num_block_elements, MPI_DOUBLE, 0, b_tag);
            hpx::wait_all(result0, result1);
        }
    }

    block_multiply(a_block, b_block, c_block, block_size);

    /* Send all result to proc 0 */
    //printf("Send all result to proc 0\n");
    {
        const int tag = 0;
        if (world_rank == 0) {
        
            hpx::future<int> f_recv0 = hpx::async(exec, MPI_Irecv, &c[blocks[1]], 1, array_block, 1, tag); 
            hpx::future<int> f_recv1 = hpx::async(exec, MPI_Irecv, &c[blocks[2]], 1, array_block, 2, tag); 
            hpx::future<int> f_recv2 = hpx::async(exec, MPI_Irecv, &c[blocks[3]], 1, array_block, 3, tag); 

            /* proc 0: copy c_block --> c0 */
            int i, j;
            for (i = 0; i < block_size; i++)
                for (j = 0; j < block_size; j++)
                    c[blocks[0] + i*N + j] = c_block[i* block_size + j];

        }
        else {
            hpx::future<int> f_send = hpx::async(exec, MPI_Isend, c_block, num_block_elements, MPI_DOUBLE, 0, tag);
        }
    }
    
    //printf("free block\n");
    free(a_block);
    free(b_block);
    free(c_block);

    /* End of paralle section */
    //printf("End of paralle section\n");
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    //printf("End of MPI timing\n");
    time += MPI_Wtime();

    if (world_rank == 0) {
        if (N <= 10) {
            printf("Input matrices:\n");
            print_matrix(a, N);
            printf("Output matrix:\n");
            print_matrix(c, N);
        }
        printf("Elapsed time: %.2lf seconds\n", time);
    }
    
    //printf("free a b c\n");
    if (world_rank == 0) {
        free(a);
        free(b);
        free(c);
        MPI_Type_free(&array_block);
    }
    
    // Stop HPX runtime 
    //printf("Stop HPX runtime \n");
    return hpx::local::finalize();
}


int main(int argc, char* argv[])
{
    // if this test is run with distributed runtime, we need to make sure
    // that all ranks run their main function
    std::vector<std::string> cfg = {"hpx.run_hpx_main!=1"};
      
    // Init MPI
    int provided = MPI_THREAD_MULTIPLE;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided != MPI_THREAD_MULTIPLE)
    {
        std::cout << "Provided MPI is not : MPI_THREAD_MULTIPLE " << provided
                  << std::endl;
    }  
     
    // Configure application-specific options.
    options_description cmdline("usage: " HPX_APPLICATION_STRING " [options]");

    // clang-format off
    cmdline.add_options()(
        "matrixsize",
        value<std::uint64_t>()->default_value(80),
        "size of matrix to test");
    // clang-format on
        
    // Initialize and run HPX.
    hpx::local::init_params init_args;
    init_args.desc_cmdline = cmdline;
    init_args.cfg = cfg;
       
    // 用户必须调用hpx::local::init，启动HPX 运行时系统，执行hpx_main和hpx线程
    auto result = hpx::local::init(hpx_main, argc, argv, init_args);  
     
    MPI_Finalize();
    
    return result || hpx::util::report_errors();
}

