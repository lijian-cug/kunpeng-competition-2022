# Block matrix multiplication using MPI
# HPX project的编译还在调试中...
## 使用了2种方法
- Point-to-point communication
- Collective communication (使用 `MPI_Scatter()` and `MPI_Gather()`)

## 编译
```
mpicc -o collective -Wall collective.c common.c
mpicc -o p2p -Wall p2p.c common.c

p2p_hpx 编译参考Makefile
```

## 运行
```
mpirun -hostfile hostfile -np 4 ./collective <matrix_size> || mpirun -np 4 ./collective <matrix_size>
mpirun -hostfile hostfile -np 4 ./p2p <matrix_size> || mpirun -np 4 ./p2p <matrix_size>
```

## 注意
只能使用4个进程mpirun运行可执行程序.
其他子进程上的MPI_Irecv，不能使用async API改写，必须Waitall()；parallel region之外的MPI_Barrier也不能使用async API改写，否则，会得到错误结果。

## 结果(秒,s） (平均运行5轮) --有待进一步测试
### On Dell, 1.8 GHz, 4 cores
| Matrix size  | P2P   | Collective |
|-------------:|------:|-----------:|
|          100 | 0.006 |      0.004 |
|          200 |  0.02 |       0.02 |
|          400 |  0.15 |      0.146 |
|          800 | 1.838 |      2.204 |
|         1000 | 5.856 |      5.692 |

### On Kunpeng920 @ 2.90GHz, 48 cores
| Matrix size  | P2P   | Collective |
|-------------:|------:|-----------:|
|          100 |  0.00 |       0.00 |
|          200 |  0.01 |       0.01 |
|          400 |  0.06 |       0.06 |
|          800 |  0.48 |       0.47 |
|         1000 |  0.96 |       0.95 |
|         2000 |  7.06 |      7.036 |

### On Dell, 1.8 GHz, 4 cores
| Matrix size  |P2P_HPX| Collective |
|-------------:|------:|-----------:|
|          100 | 0.006 |      0.004 |
|          200 |  0.02 |       0.02 |
|          400 |  0.15 |      0.146 |
|          800 | 1.838 |      2.204 |
|         1000 | 5.856 |      5.692 |

### On Kunpeng920 @ 2.90GHz, 48 cores
| Matrix size  |P2P_HPX| Collective |
|-------------:|------:|-----------:|
|          100 |  0.00 |       0.00 |
|          200 |  0.01 |       0.01 |
|          400 |  0.06 |       0.06 |
|          800 |  0.48 |       0.47 |
|         1000 |  0.96 |       0.95 |
|         2000 |  7.06 |      7.036 |
