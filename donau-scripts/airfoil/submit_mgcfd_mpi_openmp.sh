#!/bin/sh
#===========================================================
#配置DSUB资源
#===========================================================
#DSUB --job_type cosched
#DSUB -n mgcfd_mpi_openmp
#DSUB -A root.migration
#DSUB -q root.default
#DSUB -R cpu=64;mem=40960;gpu=1
#DSUB -N 1
#DSUB -oo out%J.log
#DSUB -eo err%J.log

#===========================================================
#加载环境变量
#===========================================================

module use /workspace/public/software/modules
module purge

module use /workspace/public/software/modules
module load compilers/bisheng/2.1.0/bisheng2.1.0
module use /workspace/public/software/modules
module load mpi/hmpi/1.1.1/bisheng2.1.0
module use /workspace/public/software/modules
module load compilers/cuda/11.14.1

export MY_PATH=/workspace/home/migration/zhusihai
export CUDA_INSTALL_PATH=/usr/local/cuda-11.4
export MPI_INSTALL_PATH=/workspace/public/software/mpi/hmpi/1.1.1/bisheng2.1.0/ompi
export PTSCOTCH_INSTALL_PATH=$MY_PATH/scotch_6.0.6
export PARMETIS_INSTALL_PATH=$MY_PATH/parmetis-4.0.3/metis
export HDF5_INSTALL_PATH=$MY_PATH/hdf5-hdf5-1_13_0/install
export OP2_COMPILER='clang'
export LD_LIBRARY_PATH=$CUDA_INSTALL_PATH/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PARMETIS_INSTALL_PATH/lib:$PTSCOTCH_INSTALL_PATH/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$HDF5_INSTALL_PATH/lib:$LD_LIBRARY_PATH
export OP2_INSTALL_PATH=$MY_PATH/OP2-Common-release-2020/op2/c
export PATH=/workspace/home/migration/zhusihai/MG-CFD-app-OP2-master/bin:$PATH

export OMP_PROC_BIND=true
#===========================================================
#获得hostfile
#===========================================================
echo ----- print env vars -----

if [ "${CCS_ALLOC_FILE}" != "" ]; then
    echo "   "
    ls -la ${CCS_ALLOC_FILE}
    echo ------ cat ${CCS_ALLOC_FILE}
    cat ${CCS_ALLOC_FILE}
fi

export HOSTFILE=/tmp/hostfile.$$
rm -rf $HOSTFILE
touch $HOSTFILE

# parse CCS_ALLOC_FILE
## node name,  cores, tasks, task_list
#  hpcbuild002 8 1 container_22_default_00001_e01_000002
#  hpctest005 8 1 container_22_default_00000_e01_000001

ntask=`cat ${CCS_ALLOC_FILE} | awk -v fff="$HOSTFILE" '{}
{
    split($0, a, " ")
    if (length(a[1]) >0 && length(a[3]) >0) {
        print a[1]" slots="a[2] >> fff
        total_task+=a[3]
    }
}END{print total_task}'`

echo "hypermpi hostfile $HOSTFILE generated:"
echo "-----------------------"
cat $HOSTFILE
echo "-----------------------"
echo `which mgcfd_mpi_openmp`
echo "-----------------------"
echo "Total tasks is $ntask"
echo "mpirun -hostfile $HOSTFILE -n $ntask <your application>"

#===========================================================
#运行测试脚本
#===========================================================
#mpirun -hostfile $HOSTFILE -np 2 --mca plm_rsh_agent /opt/batch/agent/tools/dstart -x LD_LIBRARY_PATH -mca pml ucx -x UCX_NET_DEVICES=mlx5_0:1 -mca btl ^vader,tcp,openib,uct -x UCX_TLS=self,sm,rc --bind-to core --map-by socket --rank-by core -x OMP_NUM_THREADS=8 mgcfd_mpi_openmp -i input.dat
mpirun -hostfile $HOSTFILE -np 1 --mca plm_rsh_agent /opt/batch/agent/tools/dstart -x LD_LIBRARY_PATH -mca pml ucx -x UCX_NET_DEVICES=mlx5_0:1 -mca btl ^vader,tcp,openib,uct -x UCX_TLS=self,sm,rc --bind-to core --map-by socket --rank-by core -x OMP_NUM_THREADS=32 airfoil_mpi_openmp

ret=$?

